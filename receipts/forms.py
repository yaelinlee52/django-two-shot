from receipts.models import Receipt, ExpenseCategory, Account
from django.forms import ModelForm

class NewReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account"
        ]

class NewExpenseCategory(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name"
        ]

class NewAccount(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number"
        ]
