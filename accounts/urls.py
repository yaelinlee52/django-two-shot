from django.urls import path
from accounts.views import account_signup, account_login, account_logout

urlpatterns = [
    path("login/", account_login, name='login'),
    path("logout/", account_logout, name="logout"),
    path("signup/", account_signup, name="signup")
]
